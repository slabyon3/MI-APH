import Vec2 from "../Vec2";
import { posix } from "path";

export class Collider {
    position: Vec2;

    // Flag-like number to specify which collider groups
    // this collider should interact with
    collidesWith: number;

    // This collider's group, used for filtering whether
    // the collision check should even be considered.
    colliderGroup: number;

    constructor(
        colliderType: number,
        collidesWith: number
    ) {
        this.colliderGroup = colliderType;
        this.collidesWith = collidesWith;
        this.position = new Vec2(0);
    }

    updateTransform(obj: PIXI.DisplayObject) {
        this.position.x = obj.x;
        this.position.y = obj.y;
    }

    checkCollision(other: Collider) {
        if (this.collidesWith & other.colliderGroup) {
            return this._checkCollisionAABB(other) || this._checkCollisionCircle(other);
        }
        return false;
    }
    
    // Not enough experience with/knowledge about TypeScript to be able
    // to write this in a nicer way
    _checkCollisionAABB(other: Collider) {
        return false; // to be overloaded by the correct subclass
    }

    _checkCollisionCircle(other: Collider) {
        return false; // to be overloaded by the correct subclass
    }

    _checkCollisionAABBCircle(other: Collider) {
        return false; // to be overloaded by the correct subclass
    }

    _checkCollisionCircleAABB(other: Collider) {
        return false; // to be overloaded by the correct subclass
    }

    _checkCollisionAABBAABB(other: Collider) {
        return false; // to be overloaded by the correct subclass
    }

    _checkCollisionCircleCircle(other: Collider) {
        return false; // to be overloaded by the correct subclass
    }
}

export class CircleCollider extends Collider {
    radius: number;

    constructor(
        colliderGroup: number,
        collidesWith: number,
        r: number
    ) {
        super(colliderGroup, collidesWith);
        this.radius = r;
    }

    _checkCollisionCircle(other: Collider) {
        return other._checkCollisionAABBCircle(this) || other._checkCollisionCircleCircle(this);
    }

    // We know here that we are a circle and at the same time the other object
    // is an AABB-type collider.
    _checkCollisionCircleAABB(other: Collider) {
        var aabb = <AABBCollider><any>other;

        var center = this.position.clone();
        if (center.x > aabb.right()) center.x = aabb.right();
        if (center.x < aabb.left()) center.x = aabb.left();
        if (center.y > aabb.bottom()) center.y = aabb.bottom();
        if (center.y < aabb.top()) center.y = aabb.top();
        
        return center.subtract(this.position).magnitudeSquared() < this.radius * this.radius;
    }

    // The internal collision check now knows 'this' is a circle
    // and 'other' is also a circle
    _checkCollisionCircleCircle(other: Collider) {
        var circle = <CircleCollider><any>other;
        var radii = this.radius + circle.radius;
        return circle.position.subtract(this.position).magnitudeSquared() < radii * radii;
    }
}

export class AABBCollider extends Collider {
    size: Vec2;
    halfSize: Vec2;

    constructor(
        colliderGroup: number,
        collidesWith: number,
        size: Vec2
    ) {
        super(colliderGroup, collidesWith);
        this.size = size;
        this.halfSize = size.divide(2);
    }

    left() {
        return this.position.x - this.halfSize.x;
    }

    right() {
        return this.position.x + this.halfSize.x;
    }

    top() {
        return this.position.y - this.halfSize.y;
    }

    bottom() {
        return this.position.y + this.halfSize.y;
    }

    _checkCollisionAABB(other: Collider) {
        return other._checkCollisionAABBAABB(this) || other._checkCollisionCircleAABB(this);
    }

    _checkCollisionAABBAABB(other: Collider) {
        var aabb = <AABBCollider><any>other;

        var horiz = Math.min(this.right(), aabb.right()) - Math.max(this.left(), aabb.left());
        var vert = Math.min(this.bottom(), aabb.bottom()) - Math.max(this.top(), aabb.top());
        return (vert > 0 && horiz > 0);
    }

    _checkCollisionAABBCircle(other: Collider) {
        return other._checkCollisionCircleAABB(this);
    }
}