
import Vec2 from './Vec2';

/**
 * Storage for aceleration and velocity 
 */
export default class Dynamics {
    static globalMaxVelocity: Vec2 = new Vec2(900, 900);

    aceleration: Vec2;
    velocity: Vec2;
    maxVelocity: Vec2;

    constructor(
        velocity = new Vec2(0, 0),
        aceleration = new Vec2(0, 0),
        maxVelocity = Dynamics.globalMaxVelocity
    ){
        this.velocity = velocity;
        this.aceleration = aceleration;
        this.maxVelocity = maxVelocity;
    }

    applyVelocity(delta: number, gameSpeed: number) {
        this.velocity = this.velocity.add(this.aceleration.multiply(delta * 0.001 * gameSpeed));
        
        if (Math.abs(this.velocity.x) > this.maxVelocity.x)
            this.velocity.x = Math.sign(this.velocity.x) * this.maxVelocity.x;
        if (Math.abs(this.velocity.y) > this.maxVelocity.y)
            this.velocity.y = Math.sign(this.velocity.y) * this.maxVelocity.y;
    }

    calcPositionChange(delta: number, gameSpeed: number): Vec2 {
        return this.velocity.multiply(delta * 0.001 * gameSpeed);
    }
}