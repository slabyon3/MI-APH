import Component from "../engine/Component";
import { MSG_OBJECT_ADDED, MSG_COLLISION, ATTR_COLLIDER, MSG_OBJECT_REMOVED, MSG_ENABLE_COLLISION, MSG_DISABLE_COLLISION } from "../engine/Constants";
import Msg from "../engine/Msg";
import { PIXICmp } from "../engine/PIXIObject";
import { Collider } from "../utils/collision/Colliders";


export class CollisionInfo {
    obj1: PIXICmp.ComponentObject;
    obj2: PIXICmp.ComponentObject;
}

export class CollisionManager extends Component {
    colliderMap: Map<PIXICmp.ComponentObject, Collider>;

    constructor() {
        super();
        this.colliderMap = new Map<PIXICmp.ComponentObject, Collider>();
    }

    onInit() {
        this.subscribe(MSG_OBJECT_ADDED, MSG_OBJECT_REMOVED, MSG_ENABLE_COLLISION, MSG_DISABLE_COLLISION);
        for (const child of this.owner.getPixiObj().children) {
            var obj = <PIXICmp.ComponentObject><any>child;
            this.tryAddCollider(obj);
        }
    }

    onMessage(msg: Msg) {
        if (msg.action == MSG_OBJECT_ADDED) {
            this.tryAddCollider(msg.gameObject);
        }
        if (msg.action == MSG_OBJECT_REMOVED) {
            this.tryRemoveCollider(msg.gameObject);
        }
        if (msg.action == MSG_ENABLE_COLLISION) {
            this.tryAddCollider(msg.data);
        }
        if (msg.action == MSG_DISABLE_COLLISION) {
            this.tryRemoveCollider(msg.data);
        }
    }

    onUpdate(delta: number, absolute: number) {
        for (const [obj1, coll1] of this.colliderMap) {
            coll1.updateTransform(obj1.getPixiObj());
            for (const [obj2, coll2] of this.colliderMap) {
                if (obj1 == obj2) continue;
                if (coll1.checkCollision(coll2)) {
                    this.sendMessage(MSG_COLLISION, <CollisionInfo>{obj1, obj2});
                }
            }
        }
    }

    private tryAddCollider(obj: PIXICmp.ComponentObject) {
        var collider = <Collider>obj.getAttribute(ATTR_COLLIDER);
        if (collider != null) {
            this.colliderMap.set(obj, collider);
        }
    }

    private tryRemoveCollider(obj: PIXICmp.ComponentObject) {
        var collider = <Collider>obj.getAttribute(ATTR_COLLIDER);
        if (collider != null) { // otherwise it didn't have a collider in the first place
            this.colliderMap.delete(obj);
        }
    }
}