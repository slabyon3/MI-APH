// message types
export const MSG_OBJECT_ADDED = "OBJECT_ADDED"; // a new object added
export const MSG_OBJECT_REMOVED = "OBJECT_REMOVED"; // old object removed
export const MSG_ANY = "MSG_ANY";  // special key for global subscribers, usually for debugging
export const MSG_STATE_CHANGED = "STATE_CHANGED"; // message emitted when a state of an object has changed
export const MSG_COMPONENT_ADDED = "COMPONENT_ADDED"; // a new component added
export const MSG_COMPONENT_REMOVED = "COMPONENT_REMOVED"; // an existing component removed
export const MSG_COLLISION = "COLLISION"; // a collision happened
export const MSG_ENABLE_COLLISION = 'ENABLE_COLLISION'; // enable collision for the object contained in the message
export const MSG_DISABLE_COLLISION = 'DISABLE_COLLISION'; // disable collision for the object contained in the message

// keys of common attributes
export const ATTR_DYNAMICS = "DYNAMICS;"
export const ATTR_COLLIDER = "COLLIDER";