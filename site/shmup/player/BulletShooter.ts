import { KeyInputComponent, KEY_X } from "../../../ts/components/KeyInputComponent";
import { checkTime } from "../Utils";
import { ShmupComponent } from "../ShmupComponent";
import { MSG_PLAYER_BULLET_SHOT } from "../Constants";

/**
 * A simple shooting component which checks if we can shoot
 * based on the set frequency and if we can, creates a new bullet.
 */
export class BulletShooter extends ShmupComponent {
    private lastShot: number = 0;

    constructor() {
        super();
    }

    onUpdate(delta: number, absolute: number) {
        // get the global component to check if our firing key is pressed
        var keys = <KeyInputComponent>this.scene.findGlobalComponentByClass(KeyInputComponent.name);
        if (keys == null)
            return;

        if (keys.isKeyPressed(KEY_X)) {
            this.tryFire(absolute);
        }
    }

    private tryFire(absolute: number) {
        if (checkTime(this.lastShot, absolute, this.model.pShootingFrequency)) {
            this.lastShot = absolute;
            this.factory.createPlayerBullet(this.owner, this.model);
            this.sendMessage(MSG_PLAYER_BULLET_SHOT);
        }
    }
}