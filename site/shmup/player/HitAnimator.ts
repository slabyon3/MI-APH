import { ShmupComponent } from "../ShmupComponent";
import { checkTime } from "../Utils";
import { BLINKING_FREQUENCY } from "../Constants";

/**
 * A component which animates when the player is hit by blinking the sprite repeatedly.
 */
export class HitAnimator extends ShmupComponent {
    lastTime: number = 0;
    remainingTime: number; // we will be blinking for this many seconds

    constructor(length: number) {
        super();
        this.remainingTime = length;
    }

    onUpdate(delta: number, absolute: number) {
        if (checkTime(this.lastTime, absolute, BLINKING_FREQUENCY)) {
            this.lastTime = absolute;
            // flip visibility to blink
            this.owner.getPixiObj().visible = !this.owner.getPixiObj().visible;
        }

        this.remainingTime -= delta / 1000;
        if (this.remainingTime <= 0) {
            // make sure the object is visible when we are done
            this.owner.getPixiObj().visible = true;
            this.finish();
        }
    }
}