import { ShmupComponent } from "../ShmupComponent";
import Dynamics from "../../../ts/utils/Dynamics";
import { ATTR_DYNAMICS } from "../../../ts/engine/Constants";
import { KeyInputComponent, KEY_UP, KEY_DOWN, KEY_LEFT, KEY_RIGHT } from "../../../ts/components/KeyInputComponent";
import Vec2 from "../../../ts/utils/Vec2";
import { isVisibleOnScene } from "../Utils";

/**
 * A movement component to allow moving the player around the stage using the arrow keys.
 */
export class PlayerMovement extends ShmupComponent {
    dynamics: Dynamics;

    onInit() {
        super.onInit();

        // cache our dynamics attribute
        this.dynamics = this.owner.getAttribute(ATTR_DYNAMICS);
    }

    onUpdate(delta: number, absolute: number) {
        var keys = <KeyInputComponent>this.scene.findGlobalComponentByClass(KeyInputComponent.name);
        if (keys == null)
            return;

        // here, 1 will be one direction and -1 will be the opposite direction,
        // which will result in the values cancelling each other should the user press both buttons
        // at the same time
        var movement = new Vec2(0, 0);

        if (keys.isKeyPressed(KEY_UP)) {
            movement.y -= 1;
        }
        if (keys.isKeyPressed(KEY_DOWN)) {
            movement.y += 1;
        }
        if (keys.isKeyPressed(KEY_LEFT)) {
            movement.x -= 1;
        }
        if (keys.isKeyPressed(KEY_RIGHT)) {
            movement.x += 1;
        }

        this.modifyVelocity(movement, delta);
    }

    /**
     * Modifies the dynamics velocity based on the set speed and user input.
     */
    private modifyVelocity(movement: Vec2, delta: number) {
        this.dynamics.velocity = new Vec2(this.model.stageSpeed, 0);

        var position = this.owner.getPixiObj().position;

        // We only want to give the player control when they are in bounds of the game.
        // Should he leave, we strip away their control and push them back in bounds.
        // The player can be almost outside the bounds, but that is considered their risk
        // since the enemies spawn off-screen.
        if (isVisibleOnScene(this.owner.getPixiObj(), this.model)) {
            this.dynamics.velocity.x += movement.x * this.model.pShipVelocity;
            this.dynamics.velocity.y += movement.y * this.model.pShipVelocity;
        } else {
            if (position.x <= this.model.stageProgress)
                this.dynamics.velocity.x += this.model.pShipVelocity;
            else
                this.dynamics.velocity.x -= this.model.pShipVelocity;

            if (position.y <= 0)
                this.dynamics.velocity.y += this.model.pShipVelocity;
            else
                this.dynamics.velocity.y -= this.model.pShipVelocity;
        }
    }
}