

import { PixiRunner } from '../../ts/PixiRunner'
import { Shmup } from './Shmup';
import { DATA_STAGE_PREFIX, DATA_STAGE_SUFFIX, TEXTURE_WARNING, TEXTURE_BACKGROUND, TEXTURE_EXPLOSION, SOUND_FIRE, SOUND_KILL, SOUND_STAGE_START, SOUND_GAMEOVER, SOUND_LIFE_LOST, TEXTURE_PLAYER, TEXTURE_LASER, TEXTURE_ASTEROID, TEXTURE_FISH, TEXTURE_SEEKER, TEXTURE_LIFE, TEXTURE_MISSILE } from './Constants';

class Components {
    engine: PixiRunner;

    // Start a new game
    constructor() {
        this.engine = new PixiRunner();

        this.engine.init(document.getElementById("gameCanvas") as HTMLCanvasElement, 1);

        PIXI.loader
            .reset()    // necessary for hot reload
            .add(TEXTURE_PLAYER, 'static/player_ship.png')
            .add(TEXTURE_LASER, 'static/laser.png')
            .add(TEXTURE_WARNING, 'static/warning.png')
            .add(TEXTURE_EXPLOSION, 'static/explosion.png')
            .add(TEXTURE_BACKGROUND, 'static/bg.jpg')
            .add(TEXTURE_ASTEROID, 'static/asteroid.png')
            .add(TEXTURE_FISH, 'static/fish.png')
            .add(TEXTURE_SEEKER, 'static/seeker.png')
            .add(TEXTURE_LIFE, 'static/life.png')
            .add(TEXTURE_MISSILE, 'static/missile.png')
            .add(SOUND_FIRE, 'static/player_shot.mp3')
            .add(SOUND_KILL, 'static/enemy_kill.mp3')
            .add(SOUND_STAGE_START, 'static/stage_start.mp3')
            .add(SOUND_GAMEOVER, 'static/game_over.mp3')
            .add(SOUND_LIFE_LOST, 'static/life_lost.mp3')
            .add(DATA_STAGE_PREFIX + 1 + DATA_STAGE_SUFFIX, 'static/stage_1.json')
            .add(DATA_STAGE_PREFIX + 2 + DATA_STAGE_SUFFIX, 'static/stage_2.json')
            .load(() => this.onAssetsLoaded());
    }

    onAssetsLoaded() {
        // load the game
        new Shmup().init(this.engine.scene);
    }
}

new Components();

