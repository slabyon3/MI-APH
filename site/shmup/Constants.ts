export const STAGE_HEIGHT = 400;
export const STAGE_WIDTH = 800;
export const ASTEROID_SPIN = 0.01;
export const ASTEROID_HALF_SPIN = ASTEROID_SPIN / 2;
export const BLINKING_FREQUENCY = 2;

export const SLOW_TEXT = 100;
export const FAST_TEXT = 1000;

// The text style will be used for all UI elements
export const UI_TEXT_STYLE = new PIXI.TextStyle({
    fontFamily: 'ka1',
    fill: '0xFFFFFF'
})

export const TEXTURE_WARNING = 'TEXTURE_WARNING';
export const TEXTURE_BACKGROUND = 'TEXTURE_BACKGROUND';
export const TEXTURE_EXPLOSION = 'TEXTURE_EXPLOSION';
export const TEXTURE_LASER = 'TEXTURE_LASER';
export const TEXTURE_PLAYER = 'TEXTURE_PLAYER';
export const TEXTURE_ASTEROID = 'TEXTURE_ASTEROID';
export const TEXTURE_FISH = 'TEXTURE_FISH';
export const TEXTURE_SEEKER = 'TEXTURE_SEEKER';
export const TEXTURE_LIFE = 'TEXTURE_LIFE';
export const TEXTURE_MISSILE = 'TEXTURE_MISSILE';

export const ANIMATION_EXPLOSION_FRAME_SIZE = 64;
export const ANIMATION_EXPLOSION_FRAMES = 16;
export const ANIMATION_PLAYER_FRAME_SIZE = 64;
export const ANIMATION_PLAYER_FRAMES = 2;
export const ANIMATION_SPEED = 20;

export const SOUND_FIRE = 'SOUND_FIRE';
export const SOUND_KILL = 'SOUND_KILL';
export const SOUND_GAMEOVER = 'SOUND_GAMEOVER';
export const SOUND_LIFE_LOST = 'SOUND_LIFE_LOST';
export const SOUND_STAGE_START = 'SOUND_STAGE_START';

export const DATA_STAGE_PREFIX = 'stage_';
export const DATA_STAGE_SUFFIX = '.json';

export const TEXT_SCORE_PREFIX = 'Score ';
export const TEXT_STAGE_PREFIX = 'Stage ';
export const TEXT_SPLASH = 'Press X to start';
export const TEXT_FINISH_PREFIX = 'Congratulations! Final score ';
export const TEXT_GAME_OVER = 'Game Over';

export const TAG_PLAYER_SHIP = 'PLAYER_SHIP';
export const TAG_PLAYER_BULLET = 'PLAYER_BULLET';
export const TAG_ASTEROID = 'ASTEROID';
export const TAG_BOSS = 'BOSS';
export const TAG_WARNING_ARROW = 'WARNING_ARROW';
export const TAG_JUMPING_FISH = 'JUMPING_FISH';
export const TAG_SEEKER = 'SEEKER';
export const TAG_MISSILE = 'MISSILE';
export const TAG_BACKGROUND = 'BACKGROUND';
export const TAG_EXPLOSION = 'EXPLOSION';

export const TAG_UI_SCORE = 'UI_SCORE';
export const TAG_UI_LIVES = 'UI_LIVES';
export const TAG_UI_SPLASH = 'UI_SPLASH';
export const TAG_UI_ANNOUNCE = 'UI_ANNOUNCE';

export const ATTR_FACTORY = 'ATTR_FACTORY';
export const ATTR_MODEL = 'ATTR_MODEL';

export const SEEKER_STATE_IDLE = 'STATE_IDLE';
export const SEEKER_STATE_AIMING = 'STATE_AIMING';
export const SEEKER_STATE_ATTACKING = 'STATE_ATTACKING';

export const MISSILE_STATE_SEEKING = 'STATE_SEEKING';
export const MISSILE_STATE_FREEFIRE = 'STATE_FREEFIRE';

export const MSG_PLAYER_BULLET_SHOT = 'PLAYER_BULLET_SHOT';
export const MSG_PLAYER_HIT = 'PLAYER_HIT';
export const MSG_ENEMY_HIT = 'ENEMY_HIT';
export const MSG_GAME_OVER = 'GAME_OVER';
export const MSG_SPAWNER_DONE = 'SPAWNER_DONE';
export const MSG_STAGE_START = 'STAGE_START';