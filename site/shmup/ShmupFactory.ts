import Scene from "../../ts/engine/Scene";
import PIXIObjectBuilder from "../../ts/builders/PIXIObjectBuilder";
import { PIXICmp } from "../../ts/engine/PIXIObject";
import { TAG_PLAYER_SHIP, ATTR_FACTORY, ATTR_MODEL, TAG_PLAYER_BULLET, TAG_ASTEROID, ASTEROID_SPIN, ASTEROID_HALF_SPIN, TAG_WARNING_ARROW, TAG_JUMPING_FISH, TAG_SEEKER, TAG_MISSILE, TEXT_SCORE_PREFIX, TAG_UI_SCORE, TAG_UI_ANNOUNCE, TAG_UI_LIVES, UI_TEXT_STYLE, TAG_UI_SPLASH, TEXT_STAGE_PREFIX, TEXT_FINISH_PREFIX, TEXT_SPLASH, TEXT_GAME_OVER, TEXTURE_WARNING, TEXTURE_BACKGROUND, TAG_BACKGROUND, TEXTURE_EXPLOSION, ANIMATION_EXPLOSION_FRAME_SIZE, ANIMATION_EXPLOSION_FRAMES, ANIMATION_SPEED, TAG_EXPLOSION, MSG_ENEMY_HIT, MSG_STAGE_START, TEXTURE_LASER, TEXTURE_PLAYER, TEXTURE_ASTEROID, TEXTURE_FISH, TEXTURE_SEEKER, ANIMATION_PLAYER_FRAMES, ANIMATION_PLAYER_FRAME_SIZE, TEXTURE_LIFE, TEXTURE_MISSILE } from "./Constants";
import { ShmupModel } from "./ShmupModel";
import { DynamicsComponent } from "../../ts/components/DynamicsComponent";
import { ATTR_DYNAMICS, ATTR_COLLIDER } from "../../ts/engine/Constants";
import Dynamics from "../../ts/utils/Dynamics";
import Vec2 from "../../ts/utils/Vec2";
import { BulletShooter } from "./player/BulletShooter";
import { KeyInputComponent, KEY_X } from "../../ts/components/KeyInputComponent";
import { StageCamera } from "./StageCamera";
import { StageAdvancer } from "./StageAdvancer";
import { PlayerMovement } from "./player/PlayerMovement";
import DebugComponent from "../../ts/components/DebugComponent";
import ChainingComponent from "../../ts/components/ChainingComponent";
import { ScreenLeaveRemover } from "./ScreenLeaveRemover";
import { CollisionManager } from "../../ts/components/CollisionManager";
import { CircleCollider, AABBCollider } from "../../ts/utils/collision/Colliders"
import { CollisionHandler } from "./CollisionHandler";
import { SpinComponent } from "./SpinComponent";
import { ScreenEnterBlocker } from "./ScreenEnterBlocker";
import { HitAnimator } from "./player/HitAnimator";
import { ScreenEdgeFollower } from "./ui/ScreenEdgeFollower";
import { SeekerBehaviour } from "./enemies/seeker/SeekerBehaviour";
import { WanderMovement } from "./enemies/seeker/WanderMovement";
import { SeekMovement } from "./enemies/missile/SeekMovement";
import { MissileBehaviour } from "./enemies/missile/MissileBehaviour";
import { ScoreUpdater } from "./ui/ScoreUpdater";
import { AnnounceMovement } from "./ui/AnnounceMovement";
import { LifeDisplay } from "./ui/LifeDisplay";
import { PositionKeeper } from "./ui/PositionKeeper";
import { StageSpawner } from "./StageSpawner";
import { GenericComponent } from "../../ts/components/GenericComponent";
import { StageOverChecker } from "./StageOverChecker";
import { AnimationComponent } from "./AnimationComponent";
import { SoundComponent } from "./SoundComponent";

/**
 * The main class for creating any game object. Handles creating obejct, inserting them into
 * the stage, switching levels and resetting the game.
 */
export class ShmupFactory {
    static globalScale = 1;

    /**
     * Initialize the level which is currently selected in the model.
     */
    initializeLevel(scene: Scene, model: ShmupModel) {
        var builder = new PIXIObjectBuilder(scene);

        //scene.addGlobalComponent(new DebugComponent(document.getElementById("debugSect")));

        // We will always need some way for the player to make an input.
        scene.addGlobalComponent(new KeyInputComponent());

        // The background is also always there.
        builder
        .anchor(0.5, 0.5)
        .relativePos(0.5, 0.5)
        .withComponent(new PositionKeeper())
        .build(new PIXICmp.Sprite(TAG_BACKGROUND, PIXI.Texture.fromImage(TEXTURE_BACKGROUND)), scene.stage);

        if (model.stageNumber == 0) {
            // The zeroth stage is the intro stage, with just a simple writing to press X.
            scene.addGlobalComponent(new GenericComponent('PressXToStart').doOnUpdate(
                (cmp, delta, abs) => {
                    var keys = <KeyInputComponent>scene.findGlobalComponentByClass(KeyInputComponent.name);
                    if (keys.isKeyPressed(KEY_X)) {
                        model.stageNumber++;
                        this.loadStage(scene, model);
                    }
                }
            ));

            var finishText = new PIXICmp.Text(TAG_UI_SPLASH, TEXT_SPLASH);
            finishText.style = UI_TEXT_STYLE;

            builder
            .anchor(0.5, 0.5)
            .relativePos(0.5, 0.5)
            .build(finishText, scene.stage);
            return;
        } else if (model.stageNumber > model.stagesTotal) {
            // If we are out of stages to load, we either are
            // in a game-over state of we are finished, which means victory.
            var text: string;
            if (model.pHealth > 0) {
                // In case of victory, also print the ending score.
                text = TEXT_FINISH_PREFIX + model.score;
            } else {
                text = TEXT_GAME_OVER;
            }
            var finishText = new PIXICmp.Text(TAG_UI_SPLASH, text);
            finishText.style = UI_TEXT_STYLE;

            builder
            .anchor(0.5, 0.5)
            .relativePos(0.5, 0.5)
            .build(finishText, scene.stage);
            return;
        }

        // Fill the root object with the necessary components...
        scene.addGlobalComponent(new StageCamera());         // a scrolling camera
        scene.addGlobalComponent(new StageAdvancer());       // a stage scroller to move the stage and advance the game
        scene.addGlobalComponent(new CollisionManager());    // a collision manager to detect collisions
        scene.addGlobalComponent(new CollisionHandler());    // a collision handle to decide how to process detected collisions
        scene.addGlobalComponent(new StageSpawner());        // a spawner to spawn various enemies at specified times and places during the stage
        scene.addGlobalComponent(new StageOverChecker());    // a checker component to see if the stage is done in one way or another
        scene.addGlobalComponent(new SoundComponent());      // a component to produce sounds based on messages
        scene.addGlobalComponent(new GenericComponent('ExplosionSpawner').doOnMessage(  // a component to produce explosions where enemies die
            MSG_ENEMY_HIT,
            (cmp, msg) => { this.createExplosion(scene, msg.data) }
        ));
        scene.addGlobalComponent(new GenericComponent('StageStartNotifier').doOnUpdate( // and a component to send a message when the stage starts to make a jingle
            (cmp, delta, abs) => {cmp.sendMessage(MSG_STAGE_START); cmp.finish();}
        ));

        // add a score display to the top left
        var scoreText = new PIXICmp.Text(TAG_UI_SCORE, TEXT_SCORE_PREFIX + model.score);
        scoreText.style = UI_TEXT_STYLE;
        builder
        .anchor(0, 0)
        .relativePos(0, 0)
        .withComponent(new ScoreUpdater())
        .withComponent(new PositionKeeper())
        .build(scoreText, scene.stage);

        // add a lives display to the bottom left
        var livesTexture = PIXI.Texture.fromImage(TEXTURE_LIFE);
        builder
        .scale(ShmupFactory.globalScale)
        .anchor(0, 1)
        .relativePos(0, 1)
        .withComponent(new LifeDisplay())
        .withComponent(new PositionKeeper())
        .build(new PIXICmp.TilingSprite(TAG_UI_LIVES, livesTexture, 0, livesTexture.height), scene.stage);

        // add our player
        this.createPlayer(scene);

        // announce the current stage using a flying text
        this.createAnnounce(scene, TEXT_STAGE_PREFIX + model.stageNumber);
    }

    /**
     * Constructs and adds a text which flies across the screen with
     * the specified message in the UI font.
     */
    createAnnounce(scene: Scene, text: string) {
        var builder = new PIXIObjectBuilder(scene);

        var announceText = new PIXICmp.Text(TAG_UI_ANNOUNCE, text);
        announceText.style = UI_TEXT_STYLE;

        builder
        .anchor(0.5, 0)
        .relativePos(1, 0.5)
        .withAttribute(ATTR_DYNAMICS, new Dynamics())
        .withComponent(new DynamicsComponent())
        .withComponent(new AnnounceMovement())
        .withComponent(new ChainingComponent()
            .addComponentAndWait(new ScreenEnterBlocker())
            .addComponent(new ScreenLeaveRemover()))
        .build(announceText, scene.stage);
    }

    /**
     * Constructs the player object consisting of a simple animated sprite and
     * various components for movement and shooting.
     */
    createPlayer(scene: Scene) {
        var builder = new PIXIObjectBuilder(scene);

        var texture = PIXI.Texture.fromImage(TEXTURE_PLAYER).clone();
        texture.frame = new PIXI.Rectangle(0, 0, ANIMATION_PLAYER_FRAME_SIZE, ANIMATION_PLAYER_FRAME_SIZE);

        builder
        .scale(ShmupFactory.globalScale)
        .anchor(0.5, 0.5)
        .relativePos(0.05, 0.5)
        .withAttribute(ATTR_DYNAMICS, new Dynamics())
        .withAttribute(ATTR_COLLIDER, new AABBCollider(0x10000000, 0x0FFFFFFF, new Vec2(60, 40)))
        .withComponent(new DynamicsComponent())
        .withComponent(new PlayerMovement())
        .withComponent(new BulletShooter())
        .withComponent(new AnimationComponent(ANIMATION_PLAYER_FRAMES, ANIMATION_SPEED, false))
        .build(new PIXICmp.Sprite(TAG_PLAYER_SHIP, texture), scene.stage);
    }

    /**
     * Constructs a single bullet shot by the player which can hit enemies. 
     */
    createPlayerBullet(ship: PIXICmp.ComponentObject, model: ShmupModel) {
        var builder = new PIXIObjectBuilder(ship.getScene());

        var pxObj = ship.getPixiObj();
        var hwidth = pxObj.getBounds().width / 2;

        builder
        .scale(ShmupFactory.globalScale)
        .anchor(0.5, 0.5)
        .localPos(pxObj.x + hwidth, pxObj.y)
        .withAttribute(ATTR_DYNAMICS, new Dynamics(new Vec2(model.pBulletVelocity, 0)))
        .withAttribute(ATTR_COLLIDER, new CircleCollider(0x20000000, 0x0FFFFFFF, 10))
        .withComponent(new DynamicsComponent())
        .withComponent(new ScreenLeaveRemover())
        .build(new PIXICmp.Sprite(TAG_PLAYER_BULLET, PIXI.Texture.fromImage(TEXTURE_LASER)), ship.getScene().stage);
    }

    /**
     * Constructs a simple, stationary asteroid which just rotates a bit.
     */
    createAsteroid(scene: Scene, position: Vec2) {
        var builder = new PIXIObjectBuilder(scene);

        var obj = builder
        .scale(ShmupFactory.globalScale)
        .anchor(0.5, 0.5)
        .localPos(position.x, position.y)
        .withAttribute(ATTR_COLLIDER, new CircleCollider(0x01000000, 0x00000000, 10))
        .withComponent(new ChainingComponent()
            .addComponentAndWait(new ScreenEnterBlocker())
            .addComponent(new ScreenLeaveRemover()))
        .withComponent(new SpinComponent(Math.random() * ASTEROID_SPIN - ASTEROID_HALF_SPIN)) // Add a bit of random spin
        .build(new PIXICmp.Sprite(TAG_ASTEROID, PIXI.Texture.fromImage(TEXTURE_ASTEROID)), scene.stage);

        this.createEdgeArrow(scene, obj);
    }

    /**
     * Constructs an arrow at the edge of the screen pointing at an incoming enemy.
     */
    createEdgeArrow(scene: Scene, target: PIXICmp.ComponentObject) {
        var builder = new PIXIObjectBuilder(scene);

        builder
        .scale(ShmupFactory.globalScale)
        .anchor(1.0, 0.5)
        .withComponent(new ScreenEdgeFollower(target))
        .build(new PIXICmp.Sprite(TAG_WARNING_ARROW, PIXI.Texture.fromImage(TEXTURE_WARNING)), scene.stage);
    }

    /**
     * Constructs the 'fish' enemy, a ship which emerges briefly from the edge of the stage and
     * goes back. A simple avoidable enemy.
     */
    createJumpingFish(scene: Scene, model: ShmupModel, position: Vec2, jumpFromTop: boolean) {
        var builder = new PIXIObjectBuilder(scene);

        var velocity = new Vec2(model.stageSpeed + model.eFishSpeed / 2, 0);
        if (jumpFromTop) velocity.y = model.eFishSpeed;
        else velocity.y = -model.eFishSpeed;

        var acceleration = new Vec2(0, 0);
        if (jumpFromTop) acceleration.y = -model.eFishAcceleration;
        else acceleration.y = model.eFishAcceleration;

        var obj = builder
        .scale(ShmupFactory.globalScale)
        .anchor(0.5, 0.5)
        .localPos(position.x, position.y)
        .withAttribute(ATTR_COLLIDER, new CircleCollider(0x01000000, 0x00000000, 10))
        .withAttribute(ATTR_DYNAMICS, new Dynamics(velocity, acceleration))
        .withComponent(new ChainingComponent()
            .addComponentAndWait(new ScreenEnterBlocker())
            .addComponent(new ScreenLeaveRemover()))
        .withComponent(new DynamicsComponent())
        .build(new PIXICmp.Sprite(TAG_JUMPING_FISH, PIXI.Texture.fromImage(TEXTURE_FISH)), scene.stage);

        this.createEdgeArrow(scene, obj);
    }

    /**
     * Constructs a 'seeker' enemy to wander and perhaps eventually run at the player.
     */
    createSeeker(scene: Scene, position: Vec2) {
        var builder = new PIXIObjectBuilder(scene);

        var obj = builder
        .scale(ShmupFactory.globalScale)
        .anchor(0.5, 0.5)
        .localPos(position.x, position.y)
        .withAttribute(ATTR_COLLIDER, new CircleCollider(0x01000000, 0x00000000, 10))
        .withAttribute(ATTR_DYNAMICS, new Dynamics())
        .withComponent(new ChainingComponent()
            .addComponentAndWait(new ScreenEnterBlocker())
            .addComponent(new ScreenLeaveRemover()))
        .withComponent(new DynamicsComponent())
        .withComponent(new WanderMovement())
        .withComponent(new SeekerBehaviour())
        .build(new PIXICmp.Sprite(TAG_SEEKER, PIXI.Texture.fromImage(TEXTURE_SEEKER)), scene.stage);

        this.createEdgeArrow(scene, obj);
    }

    /**
     * Constructs a 'missile' enemy to chase the player.
     */
    createMissile(scene: Scene, position: Vec2) {
        var builder = new PIXIObjectBuilder(scene);

        var obj = builder
        .scale(ShmupFactory.globalScale)
        .anchor(0.5, 0.5)
        .localPos(position.x, position.y)
        .withAttribute(ATTR_COLLIDER, new CircleCollider(0x00100000, 0x0F0FFFFF, 10))
        .withAttribute(ATTR_DYNAMICS, new Dynamics(new Vec2(0), new Vec2(0), new Vec2(200, 200)))
        .withComponent(new ChainingComponent()
            .addComponentAndWait(new ScreenEnterBlocker())
            .addComponent(new ScreenLeaveRemover()))
        .withComponent(new DynamicsComponent())
        .withComponent(new SeekMovement(scene.findAllObjectsByTag(TAG_PLAYER_SHIP)[0]))
        .withComponent(new MissileBehaviour())
        .build(new PIXICmp.Sprite(TAG_MISSILE, PIXI.Texture.fromImage(TEXTURE_MISSILE)), scene.stage);

        this.createEdgeArrow(scene, obj);
    }

    /**
     * Construct an animated explosion ath the specified position.
     */
    createExplosion(scene: Scene, position: Vec2) {
        var builder = new PIXIObjectBuilder(scene);

        var texture = PIXI.Texture.fromImage(TEXTURE_EXPLOSION).clone();
        texture.frame = new PIXI.Rectangle(0, 0, ANIMATION_EXPLOSION_FRAME_SIZE, ANIMATION_EXPLOSION_FRAME_SIZE);

        var obj = builder
        .scale(ShmupFactory.globalScale)
        .anchor(0.5, 0.5)
        .localPos(position.x, position.y)
        .withComponent(new ChainingComponent()
            .addComponentAndWait(new AnimationComponent(ANIMATION_EXPLOSION_FRAMES, ANIMATION_SPEED, true))
            .execute(() => {obj.remove()}))
        .build(new PIXICmp.Sprite(TAG_EXPLOSION, texture), scene.stage);
    }

    /**
     * Loads the current stage selected in the model.
     */
    loadStage(scene: Scene, model: ShmupModel) {
        scene.clearScene();
        scene.addGlobalAttribute(ATTR_FACTORY, this);
        scene.addGlobalAttribute(ATTR_MODEL, model);
        model.stageProgress = 0;
        this.initializeLevel(scene, model);
    }
    
    /**
     * Resets the game completely.
     */
    resetGame(scene: Scene) {
        scene.clearScene();
        scene.addGlobalAttribute(ATTR_FACTORY, this);

        let model = new ShmupModel();
        scene.addGlobalAttribute(ATTR_MODEL, model);
        this.initializeLevel(scene, model);
    }
}