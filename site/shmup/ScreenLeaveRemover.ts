import { ShmupComponent } from "./ShmupComponent";
import { isVisibleOnScene } from "./Utils";

/**
 * A simple component which removes the object once it leaves the stage.
 */
export class ScreenLeaveRemover extends ShmupComponent {
    onUpdate(delta: number, absolute: number) {
        if (!isVisibleOnScene(this.owner.getPixiObj(), this.model)) {
            this.owner.remove();
        }
    }
}