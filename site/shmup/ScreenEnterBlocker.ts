import { ShmupComponent } from "./ShmupComponent";
import { isVisibleOnScene } from "./Utils";

/**
 * A simple component which finishes when it's owner enters the screen.
 * Used in conjunction with the chaining component.
 */
export class ScreenEnterBlocker extends ShmupComponent {
    onUpdate(delta: number, absolute: number) {
        if (isVisibleOnScene(this.owner.getPixiObj(), this.model)) {
            this.finish();
        }
    }
}