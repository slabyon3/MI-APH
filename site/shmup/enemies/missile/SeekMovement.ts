import { ShmupComponent } from "../../ShmupComponent";
import Dynamics from "../../../../ts/utils/Dynamics";
import { SteeringMath } from "../../../../ts/utils/SteeringMath";
import Vec2 from "../../../../ts/utils/Vec2";
import { ATTR_DYNAMICS } from "../../../../ts/engine/Constants";
import { PIXICmp } from "../../../../ts/engine/PIXIObject";

/**
 * A movement component for the missile which uses the 'seek' steering
 * behaviour to aim at the specified target.
 */
export class SeekMovement extends ShmupComponent {
    target: PIXICmp.ComponentObject;
    dynamics: Dynamics;
    steeringMath: SteeringMath = new SteeringMath();

    constructor(target: PIXICmp.ComponentObject) {
        super();
        this.target = target;
    }

    onInit() {
        super.onInit();
        // Cache our dynamics component and set its initial velocity to avoid NaN in the seek code.
        this.dynamics = this.owner.getAttribute(ATTR_DYNAMICS);
        this.dynamics.velocity = new Vec2(-1, 0);
    }

    onUpdate(delta: number, absolute: number) {
        // Get the required information and compute the seeking force
        // using the algorithm provided in the engine.
        var targetPos = new Vec2(this.target.getPixiObj().x, this.target.getPixiObj().y);
        var position = new Vec2(this.owner.getPixiObj().x, this.owner.getPixiObj().y);
        var force = this.steeringMath.seek(
            targetPos,
            position,
            this.dynamics.velocity,
            this.dynamics.maxVelocity.x,
            this.model.eMissileStopRadius);

        this.dynamics.velocity = this.dynamics.velocity.add(force);

        // Apart from updating our velocity, also update our rotation based on our
        // current velocity.
        var direction = this.dynamics.velocity.normalize();
        var rotation = Math.atan2(direction.y, direction.x);
        this.owner.getPixiObj().rotation = rotation;
    }
}