import { ShmupComponent } from "../../ShmupComponent";
import Component from "../../../../ts/engine/Component";
import { SeekMovement } from "./SeekMovement";
import { MISSILE_STATE_SEEKING, MISSILE_STATE_FREEFIRE, TAG_PLAYER_SHIP } from "../../Constants";
import Vec2 from "../../../../ts/utils/Vec2";

/**
 * A basic state-machine based behaviour for the 'missile' enemy.
 * Targets the player but allows dodging 'at the last second'.
 */
export class MissileBehaviour extends ShmupComponent {
    lastState = MISSILE_STATE_SEEKING;
    state = MISSILE_STATE_SEEKING;

    movementComponent: Component;

    onInit() {
        super.onInit();
        // cache the current movement component to remove it later
        this.movementComponent = this.owner.findComponentByClass(SeekMovement.name);
    }

    onUpdate(delta: number, absolute: number) {
        var stateChange = this.state != this.lastState;
        this.lastState = this.state;

        switch(this.state) {
            case MISSILE_STATE_SEEKING:
                this.state = this.processSeekingState(stateChange, delta, absolute);
                break;
            case MISSILE_STATE_FREEFIRE:
                this.state = this.processFreefireState(stateChange, delta, absolute);
                break;
        }
    }

    /**
     * Process the 'seeking' state in which the missile seeks or follows the player.
     * When near enough, advance to the next state.
     */
    private processSeekingState(stateChange: boolean, delta: number, absolute: number) {
        var player = this.scene.findAllObjectsByTag(TAG_PLAYER_SHIP)[0].getPixiObj();
        var selfpixi = this.owner.getPixiObj();

        // get both positions into vectors for easier manipulation
        var pPos = new Vec2(player.x, player.y);
        var sPos = new Vec2(selfpixi.x, selfpixi.y);

        // get the current distance
        var dist = pPos.subtract(sPos).magnitudeSquared();

        // decide whether to advance to the next state
        if (dist <= this.model.eMissileStopRadius * this.model.eMissileStopRadius) {
            return MISSILE_STATE_FREEFIRE;
        }
        return MISSILE_STATE_SEEKING
    }

    /**
     * The 'freefire' state just removes the current movement component
     * and lets the dynamics component continue moving the object in its current direction.
     */
    private processFreefireState(stateChange: boolean, delta: number, absolute: number) {
        var player = this.scene.findAllObjectsByTag(TAG_PLAYER_SHIP)[0];

        if (stateChange) {
            this.owner.removeComponent(this.movementComponent);
            // just maintain current velocity going straight
        }
        
        return MISSILE_STATE_FREEFIRE;
    }
}