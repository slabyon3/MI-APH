import { ShmupComponent } from "../../ShmupComponent";
import { ATTR_DYNAMICS } from "../../../../ts/engine/Constants";
import Dynamics from "../../../../ts/utils/Dynamics";
import Vec2 from "../../../../ts/utils/Vec2";
import Component from "../../../../ts/engine/Component";
import { PIXICmp } from "../../../../ts/engine/PIXIObject";

/**
 * A movement component for the 'seeker' enemy which rotates
 * the object so that is aims directly at the target.
 */
export class AimingMovement extends ShmupComponent {
    dynamics: Dynamics;
    aimingSpeed: number;
    target: PIXICmp.ComponentObject;

    constructor(aimingSpeed: number, target: PIXICmp.ComponentObject) {
        super();
        this.target = target;
        this.aimingSpeed = aimingSpeed;
    }

    onInit() {
        super.onInit();
        // Make sure we aren'd moving, our only purpose is roration.
        this.dynamics = this.owner.getAttribute(ATTR_DYNAMICS);
        this.dynamics.aceleration = new Vec2(0);
        this.dynamics.velocity = new Vec2(this.model.stageSpeed, 0);
    }

    onUpdate(delta: number, absolute: number) {
        var pxObj = this.target.getPixiObj();
        var position = new Vec2(this.owner.getPixiObj().x, this.owner.getPixiObj().y);

        // get our target direction and rotation to compute the difference between that
        // and our current value
        var direction = new Vec2(pxObj.x, pxObj.y).subtract(position).normalize();
        var targetRotation = Math.atan2(direction.y, direction.x);

        var rotationDelta = targetRotation - this.owner.getPixiObj().rotation;
        // correct the angle for the 0/2PI bridge where the target being on the other
        // side would normally cause us to make a full rotation around the other side
        if (rotationDelta > Math.PI) rotationDelta -= Math.PI * 2;
        if (rotationDelta < -Math.PI) rotationDelta += Math.PI * 2;
        rotationDelta = rotationDelta * this.aimingSpeed;

        // make sure to apply the delta time
        this.owner.getPixiObj().rotation += rotationDelta * delta / 1000;
    }
}