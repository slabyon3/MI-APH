import { ShmupComponent } from "../../ShmupComponent";
import Dynamics from "../../../../ts/utils/Dynamics";
import { ATTR_DYNAMICS } from "../../../../ts/engine/Constants";
import Vec2 from "../../../../ts/utils/Vec2";
import { SteeringMath } from "../../../../ts/utils/SteeringMath"

/**
 * A simple movement component which uses the wander steering algorithm
 * to go around aimlessly.
 */
export class WanderMovement extends ShmupComponent {
    dynamics: Dynamics;
    steeringMath: SteeringMath = new SteeringMath();
    wanderTarget: Vec2 = new Vec2(0, 0);

    onInit() {
        super.onInit();
        // Cache dynamics, set current velocity to avoid NaN.
        this.dynamics = this.owner.getAttribute(ATTR_DYNAMICS);
        this.dynamics.velocity = new Vec2(1, 0);
    }

    onUpdate(delta: number, absolute: number) {
        // find the new velocity and apply it
        var velocity = this.steeringMath.wander(
            this.dynamics,
            this.wanderTarget,
            this.model.eSeekerWanderRadius,
            this.model.eSeekerWanderDistance,
            this.model.eSeekerWanderJitter,
            delta);
        // also compute our new rotation
        var direction = velocity.normalize();
        var rotation = Math.atan2(direction.y, direction.x);

        this.owner.getPixiObj().rotation = rotation;
        this.dynamics.velocity = velocity;
    }
}