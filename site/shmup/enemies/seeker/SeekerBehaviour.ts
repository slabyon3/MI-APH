import { ShmupComponent } from "../../ShmupComponent";
import { SEEKER_STATE_IDLE, SEEKER_STATE_AIMING, SEEKER_STATE_ATTACKING, TAG_PLAYER_SHIP } from "../../Constants";
import Vec2 from "../../../../ts/utils/Vec2";
import { WanderMovement } from "./WanderMovement";
import Component from "../../../../ts/engine/Component";
import { ATTR_DYNAMICS } from "../../../../ts/engine/Constants";
import Dynamics from "../../../../ts/utils/Dynamics";
import { AimingMovement } from "./AimingMovement";

/**
 * A behaviour component for the 'seeker' enemy. It's simple state machine goes
 * Idle -> Aiming -> Attacking. Aims when it 'sees' the player and starts attacking,
 * which is basically just straight movement, when the rotation is considered finished.
 */
export class SeekerBehaviour extends ShmupComponent {
    lastState = SEEKER_STATE_IDLE;
    state = SEEKER_STATE_IDLE;

    movementComponent: Component;

    onInit() {
        super.onInit();
        // cache our current movement component, we will be changing it later
        this.movementComponent = this.owner.findComponentByClass(WanderMovement.name);
    }

    onUpdate(delta: number, absolute: number) {
        var stateChange = this.state != this.lastState;
        this.lastState = this.state;

        switch(this.state) {
            case SEEKER_STATE_IDLE:
                this.state = this.processIdleState(stateChange, delta, absolute);
                break;
            case SEEKER_STATE_AIMING:
                this.state = this.processAimingState(stateChange, delta, absolute);
                break;
            case SEEKER_STATE_ATTACKING:
                this.state = this.processAttackingState(stateChange, delta, absolute);
                break;
        }
    }

    /**
     * Process the idle state in which we wander (using the current movement component) and
     * try seeing if the player is in out 'line of sight'.
     */
    private processIdleState(stateChange: boolean, delta: number, absolute: number) {
        var player = this.scene.findAllObjectsByTag(TAG_PLAYER_SHIP)[0].getPixiObj();
        var selfpixi = this.owner.getPixiObj();

        var pPos = new Vec2(player.x, player.y);
        var sPos = new Vec2(selfpixi.x, selfpixi.y);

        // compute our direction and the direction from us to the player
        var directionToPlayer = pPos.subtract(sPos).normalize();
        var direction = new Vec2(Math.cos(selfpixi.rotation), Math.sin(selfpixi.rotation)).normalize();

        if (directionToPlayer.subtract(direction).magnitudeSquared() < 1) { // roughly equals the seeker's vision cone angle
            return SEEKER_STATE_AIMING;
        }
        return SEEKER_STATE_IDLE;
    }

    /**
     * The aiming state first changes the movement component to the aiming one so that we actually
     * start aiming. Then we just keep checking if we are aimed 'enough'.
     */
    private processAimingState(stateChange: boolean, delta: number, absolute: number) {
        var player = this.scene.findAllObjectsByTag(TAG_PLAYER_SHIP)[0];

        if (stateChange) {
            this.owner.removeComponent(this.movementComponent);
            this.movementComponent = new AimingMovement(this.model.eSeekerAimingSpeed, player);
            this.owner.addComponent(this.movementComponent);
        }

        // compute the current directions and rotations arrive at the value we still need to rotate
        // in order to be 'aimed' at the player
        var pxObj = player.getPixiObj();
        var position = new Vec2(this.owner.getPixiObj().x, this.owner.getPixiObj().y);
        var direction = new Vec2(pxObj.x, pxObj.y).subtract(position).normalize();
        var targetRotation = Math.atan2(direction.y, direction.x);
        var rotationDifference = targetRotation - this.owner.getPixiObj().rotation;
        rotationDifference = rotationDifference % (Math.PI * 2);

        if (rotationDifference <= this.model.eSeekerAimingTolerance) {
            return SEEKER_STATE_ATTACKING; // aiming done, time to attack
        }

        return SEEKER_STATE_AIMING;
    }

    /**
     * The attacking state is just straight accelerating towards the player.
     */
    private processAttackingState(stateChange: boolean, delta: number, absolute: number) {
        if (stateChange) {
            // remove the movement component, we won't be needing it anymore
            this.owner.removeComponent(this.movementComponent);
            var selfpixi = this.owner.getPixiObj();
            var direction = new Vec2(Math.cos(selfpixi.rotation), Math.sin(selfpixi.rotation));
            var acceleration = direction.multiply(this.model.eSeekerAcceleration);

            var dynamics = <Dynamics>this.owner.getAttribute(ATTR_DYNAMICS);
            // setting the acceleration is enough, the dynamics component will handle the rest
            dynamics.aceleration = acceleration;
        }

        return SEEKER_STATE_ATTACKING;
    }
}