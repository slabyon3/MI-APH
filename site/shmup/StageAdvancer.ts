import { ShmupComponent } from "./ShmupComponent";

/**
 * A simple component to gradually advance the (model) stage
 * at the specified rate.
 */
export class StageAdvancer extends ShmupComponent {
    onUpdate(delta: number, absolute: number) {
        var progress = this.model.stageSpeed * delta / 1000;
        var position = this.model.stageProgress + progress;

        this.model.stageProgress = position;
    }
}