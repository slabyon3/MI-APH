import Scene from "../../ts/engine/Scene";
import { ShmupModel } from "./ShmupModel";
import { STAGE_HEIGHT, STAGE_WIDTH } from "./Constants";

/**
 * Returns true if given time has already reached or exceeded certain period 
 */
export function checkTime(lastTime, time, frequency) {
    return (time - lastTime) > 1000 / frequency;
}

/**
 * Returns true if the given PIXI object is still at least partially visible on the screen
 */
export function isVisibleOnScene(obj: PIXI.DisplayObject, model: ShmupModel) {
    var objBounds = obj.getBounds();
    if (objBounds.bottom <= 0)
        return false;
    if (objBounds.top >= STAGE_HEIGHT)
        return false;
    
    var xStart = model.stageProgress;
    var xEnd = model.stageProgress + STAGE_WIDTH;
    if (objBounds.right <= 0)
        return false;
    if (objBounds.left >= STAGE_WIDTH)
        return false;

    return true;
}