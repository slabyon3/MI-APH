import Component from "../../ts/engine/Component";
import { ATTR_FACTORY, ATTR_MODEL } from "./Constants";
import { ShmupFactory } from "./ShmupFactory";
import { ShmupModel } from "./ShmupModel";

/**
 * A base-component with the model and the factory cached for convenience.
 */
export class ShmupComponent extends Component {
    model: ShmupModel;
    factory: ShmupFactory;

    onInit() {
        this.model = this.scene.getGlobalAttribute<ShmupModel>(ATTR_MODEL);
        this.factory = this.scene.getGlobalAttribute<ShmupFactory>(ATTR_FACTORY);
    }

}