import Scene from '../../ts/engine/Scene';

import { KeyInputComponent } from '../../ts/components/KeyInputComponent';
import { ShmupFactory } from './ShmupFactory';
import { ATTR_FACTORY } from './Constants';

/**
 * The game class itself. Just delegate to the factory for actual initialization.
 */
export class Shmup {
    init(scene: Scene) {
        let factory = new ShmupFactory();
        factory.resetGame(scene);
    }
}