import { ShmupComponent } from "./ShmupComponent";
import { MSG_COLLISION, MSG_DISABLE_COLLISION, MSG_ENABLE_COLLISION } from "../../ts/engine/Constants";
import Msg from "../../ts/engine/Msg";
import { CollisionInfo } from "../../ts/components/CollisionManager";
import { TAG_PLAYER_SHIP, TAG_BOSS, MSG_GAME_OVER, MSG_PLAYER_HIT, MSG_ENEMY_HIT, TAG_PLAYER_BULLET } from "./Constants";
import { PIXICmp } from "../../ts/engine/PIXIObject";
import ChainingComponent from "../../ts/components/ChainingComponent";
import { HitAnimator } from "./player/HitAnimator";
import Vec2 from "../../ts/utils/Vec2";

/**
 * Handles collisions detected by the collision manager. Sends messages
 * based on the type of collision detected (the objects involved).
 */
export class CollisionHandler extends ShmupComponent {
    onInit() {
        super.onInit();

        this.subscribe(MSG_COLLISION);
    }

    onMessage(msg: Msg) {
        if (msg.action == MSG_COLLISION) {
            var cinfo = <CollisionInfo>msg.data;

            // check who is involved in the collision
            // and decide what to do accordingly
            if (cinfo.obj1.getTag() == TAG_PLAYER_SHIP)
                this.handlePlayerHit(cinfo.obj1);
            else
                this.handleHit(cinfo.obj1);
            if (cinfo.obj2.getTag() == TAG_PLAYER_SHIP)
                this.handlePlayerHit(cinfo.obj2);
            else
                this.handleHit(cinfo.obj2);
        }
    }

    /**
     * Handle the player being involved in a collision. That involves
     * making the player invulnerable for a brief period along with an animation.
     */
    private handlePlayerHit(player: PIXICmp.ComponentObject) {
        this.model.pHealth--;

        // the deregistration, animation and reregistration is handled by a chaining component
        player.addComponent(
            new ChainingComponent()
            .execute(comp => { comp.sendMessage(MSG_DISABLE_COLLISION, comp.owner); })
            .addComponentAndWait(new HitAnimator(this.model.pInvincibilityLength))
            .execute(comp => { comp.sendMessage(MSG_ENABLE_COLLISION, comp.owner); }));

        if (this.model.pHealth <= 0) {
            // make sure to send a game-over instead if the player is out of lives
            this.sendMessage(MSG_GAME_OVER);
        } else {
            this.sendMessage(MSG_PLAYER_HIT);
        }
    }

    /**
     * Handly anything else being hit by removing it and sending a message about it.
     * If the thing was a player's bullet, don't send any message.
     */
    private handleHit(obj: PIXICmp.ComponentObject) {
        if (obj.getTag() != TAG_PLAYER_BULLET)
            this.sendMessage(MSG_ENEMY_HIT, new Vec2(obj.getPixiObj().x, obj.getPixiObj().y));
        obj.remove();
    }
}