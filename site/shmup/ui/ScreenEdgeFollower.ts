import { ShmupComponent } from "../ShmupComponent";
import { PIXICmp } from "../../../ts/engine/PIXIObject";
import { isVisibleOnScene } from "../Utils";
import Vec2 from "../../../ts/utils/Vec2";
import { STAGE_WIDTH, STAGE_HEIGHT } from "../Constants";

/**
 * A component which snaps an object's position and rotation to the
 * edge of the stage, based on the target's position. Used for a warning arrow
 * about incoming enemies from off-screen.
 */
export class ScreenEdgeFollower extends ShmupComponent {
    target: PIXICmp.ComponentObject;

    constructor(toFollow: PIXICmp.ComponentObject) {
        super();
        this.target = toFollow;
    }

    onUpdate(delta: number, absolute: number) {
        if (isVisibleOnScene(this.target.getPixiObj(), this.model)) {
            // When the target object is finally visible, the arrow is no longer needer.
            this.owner.remove();
        }

        var pxObj = this.target.getPixiObj();
        var position = new Vec2(pxObj.x, pxObj.y);

        // clip the position to the screen edge
        if (position.x < this.model.stageProgress) position.x = this.model.stageProgress;
        if (position.x > this.model.stageProgress + STAGE_WIDTH) position.x = this.model.stageProgress + STAGE_WIDTH;
        if (position.y < 0) position.y = 0;
        if (position.y > STAGE_HEIGHT) position.y = STAGE_HEIGHT;

        // We want to be pointing towards the incoming object, so rotate based on our computed
        // position and position of the target.
        var direction = new Vec2(pxObj.x, pxObj.y).subtract(position).normalize();
        var rotation = Math.atan2(direction.y, direction.x);

        this.owner.getPixiObj().position.set(position.x, position.y);
        this.owner.getPixiObj().rotation = rotation;
    }
}