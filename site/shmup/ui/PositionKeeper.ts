import { ShmupComponent } from "../ShmupComponent";
import Vec2 from "../../../ts/utils/Vec2";

/**
 * Due to a choice made at the start of the development,
 * this component is needed to keep UI elements in their place, since the stage
 * is constantly moving to one side.
 */
export class PositionKeeper extends ShmupComponent {
    initialPosition: Vec2;

    onInit() {
        super.onInit();

        // the position at which, relative to the stage's current position, we will try
        // to keep this object
        this.initialPosition = new Vec2(this.owner.getPixiObj().x, this.owner.getPixiObj().y);
    }
    
    onUpdate(delta: number, absolute: number) {
        var pxObj = this.owner.getPixiObj();

        // correct the object's position
        pxObj.x = this.initialPosition.x + this.model.stageProgress;
    }
}