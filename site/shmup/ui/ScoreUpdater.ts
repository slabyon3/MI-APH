import { ShmupComponent } from "../ShmupComponent";
import { PIXICmp } from "../../../ts/engine/PIXIObject";
import { MSG_ENEMY_HIT, TEXT_SCORE_PREFIX } from "../Constants";
import Msg from "../../../ts/engine/Msg";

/**
 * A component to update our score and the displaying object to which it is attached.
 */
export class ScoreUpdater extends ShmupComponent {
    onInit() {
        super.onInit();
        this.subscribe(MSG_ENEMY_HIT);
    }

    onMessage(msg: Msg) {
        if (msg.action == MSG_ENEMY_HIT) {
            this.model.score += this.model.eKillScore;
            var text = <PIXICmp.Text>this.owner;

            // this part used to be a separate element but due to incorrect order
            // of components receiving the message, we would always have a lower score here.
            text.text = TEXT_SCORE_PREFIX + this.model.score;
        }
    }
}