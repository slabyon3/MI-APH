import { ShmupComponent } from "../ShmupComponent";
import { PIXICmp } from "../../../ts/engine/PIXIObject";
import { MSG_PLAYER_HIT } from "../Constants";
import Msg from "../../../ts/engine/Msg";

/**
 * Component used to display remaining lives using a tiling sprite.
 */
export class LifeDisplay extends ShmupComponent {
    display: PIXICmp.TilingSprite;

    onInit() {
        super.onInit();
        this.subscribe(MSG_PLAYER_HIT);
        this.display = <PIXICmp.TilingSprite>this.owner;

        // since we use a tiling sprite, we only need to stretch it according to the player's health
        this.display.width = this.model.pHealth * this.display.texture.width;
    }

    onMessage(msg: Msg) {
        if (msg.action == MSG_PLAYER_HIT) {
            // and update when appropriate
            this.display.width = this.model.pHealth * this.display.texture.width;
        }
    }
}