import { ShmupComponent } from "../ShmupComponent";
import Dynamics from "../../../ts/utils/Dynamics";
import { ATTR_DYNAMICS } from "../../../ts/engine/Constants";
import Vec2 from "../../../ts/utils/Vec2";
import { STAGE_WIDTH, SLOW_TEXT, FAST_TEXT } from "../Constants";

/**
 * The movement component for middle-screen announcement texts.
 */
export class AnnounceMovement extends ShmupComponent {
    dynamics: Dynamics;

    onInit() {
        super.onInit();
        this.dynamics = <Dynamics>this.owner.getAttribute(ATTR_DYNAMICS);
    }


    onUpdate(delta: number, absolute: number) {
        var position = new Vec2(this.owner.getPixiObj().x, this.owner.getPixiObj().y);
        var relX = (position.x - this.model.stageProgress) / STAGE_WIDTH;

        // slow down roughly near the middle of the screen, speed up elsewhere, mainly for visual effect
        if (Math.abs(0.5 - relX) <= 0.05) {
            this.dynamics.velocity.x = -SLOW_TEXT + this.model.stageSpeed;
        } else {
            this.dynamics.velocity.x = -FAST_TEXT + this.model.stageSpeed;
        }
        
    }
}