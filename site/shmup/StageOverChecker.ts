import { ShmupComponent } from "./ShmupComponent";
import { MSG_SPAWNER_DONE, TAG_ASTEROID, TAG_SEEKER, TAG_JUMPING_FISH, TAG_MISSILE, MSG_GAME_OVER } from "./Constants";
import Msg from "../../ts/engine/Msg";

/**
 * Checker component which waits for the spawner to finish spawning,
 * after which it waits for all enemies to disappear before sending a message
 * about the stage being finished. Also handles game-over situtations.
 */
export class StageOverChecker extends ShmupComponent {
    watchEnemies: boolean = false;
    gameOver: boolean = false;

    onInit() {
        super.onInit();
        this.subscribe(MSG_SPAWNER_DONE, MSG_GAME_OVER);
    }

    onMessage(msg: Msg) {
        if (msg.action == MSG_SPAWNER_DONE) {
            // since the spawner is finished, we will now watch for
            // when all enemies are gone
            this.watchEnemies = true;
        }
        if (msg.action == MSG_GAME_OVER) {
            this.gameOver = true;
        }
    }

    onUpdate(delta: number, absolute: number) {
        if (this.gameOver) {
            // The player is dead, load the game-over stage.
            this.model.stageNumber = this.model.stagesTotal;
            this.factory.loadStage(this.scene, this.model);
        }
        if (this.watchEnemies) {
            var asteroids = this.scene.findAllObjectsByTag(TAG_ASTEROID);
            var seekers = this.scene.findAllObjectsByTag(TAG_SEEKER);
            var fishes = this.scene.findAllObjectsByTag(TAG_JUMPING_FISH);
            var missiles = this.scene.findAllObjectsByTag(TAG_MISSILE);

            if (asteroids.length <= 0 &&
                seekers.length <= 0 &&
                fishes.length <= 0 &&
                missiles.length <= 0) {
                // The stage is completed, load the next one.
                this.model.stageNumber++;
                this.factory.loadStage(this.scene, this.model);
            }
        }
    }
}