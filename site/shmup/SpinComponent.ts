import Component from "../../ts/engine/Component"

/**
 * A simple controller to make an object spin at a constant
 * speed.
 */
export class SpinComponent extends Component {
    rotationSpeed: number;

    constructor(rotationSpeed: number) {
        super();
        this.rotationSpeed = rotationSpeed;
    }

    onUpdate(delta: number, absolute: number) {
        var obj = this.owner.getPixiObj();
        obj.rotation += delta * this.rotationSpeed;
    }
}