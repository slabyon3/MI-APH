export class ShmupModel {
    // ====== DYNAMIC DATA =========
    stageProgress = 0;
    stageNumber = 0;
    score = 0;

    pHealth = 3;
    // =============================

    // ====== STATIC DATA ==========
    stageSpeed = 40;
    stagesTotal = 2;

    // player settings
    pBulletVelocity = 400;
    pShootingFrequency = 1;
    pShipVelocity = 150;
    pInvincibilityLength = 4;

    // enemy settings
    eBulletVeloticy = 200;
    eKillScore = 10;

    // 'jumping fish' enemy
    eFishSpeed = 300;
    eFishAcceleration = 180;

    // 'seeker' enemy
    eSeekerWanderRadius = 2;
    eSeekerWanderDistance = 35;
    eSeekerWanderJitter = 0.03;
    eSeekerAimingSpeed = 10;
    eSeekerAimingTolerance = 0.01;
    eSeekerAcceleration = 200;

    // 'missile' enemy
    eMissileStopRadius = 140;
    // =============================
}