import { ShmupComponent } from "./ShmupComponent";

/**
 * A simple component to move the stage according to the
 * progress of the model.
 */
export class StageCamera extends ShmupComponent {
    onUpdate(delta: number, absolute: number) {
        var pos = this.model.stageProgress;

        // Move the camera along as the stage progresses
        this.scene.stage.getPixiObj().position.set(-pos, 0);
    }
}