import { ShmupComponent } from "./ShmupComponent";
import { SpawnInfo, SPAWN_TYPE_ASTEROID, SPAWN_TYPE_FISH, SPAWN_TYPE_MISSILE, SPAWN_TYPE_SEEKER } from "./SpawnInfo";
import { DATA_STAGE_PREFIX, DATA_STAGE_SUFFIX, MSG_SPAWNER_DONE } from "./Constants";

/**
 * Spawner component which provides the player with enemies to destroy.
 */
export class StageSpawner extends ShmupComponent {
    spawnInfo: SpawnInfo[];
    spawnProgress: number = 0;

    onInit() {
        super.onInit();

        // since the PIXI loader already nicely parses the JSON file for us, we just need to cast it
        // into an array of spawn data.
        this.spawnInfo = <SpawnInfo[]>PIXI.loader.resources[DATA_STAGE_PREFIX + this.model.stageNumber + DATA_STAGE_SUFFIX].data;
    }

    onUpdate(delta: number, absolute: number) {
        if (this.spawnInfo.length > this.spawnProgress) {
            var currentSpawn = this.spawnInfo[this.spawnProgress];
            if (this.model.stageProgress >= currentSpawn.when) {
                // spawn the next thing to spawn, since we are at or passed its
                // spawn point
                this.performSpawn(currentSpawn);
                this.spawnProgress++;
            }
            if (this.spawnInfo.length <= this.spawnProgress) {
                // nothing more to spawn, send a message about that.
                this.sendMessage(MSG_SPAWNER_DONE);
            }
        }
    }

    /**
     * Decides which creation method of the factory to use based
     * on the spawn type.
     */
    private performSpawn(spawn: SpawnInfo) {
        if (spawn.what == SPAWN_TYPE_ASTEROID) {
            this.factory.createAsteroid(this.scene, spawn.where);
        } else if (spawn.what == SPAWN_TYPE_FISH) {
            this.factory.createJumpingFish(this.scene, this.model, spawn.where, spawn.where.x < 0);
        } else if (spawn.what == SPAWN_TYPE_MISSILE) {
            this.factory.createMissile(this.scene, spawn.where);
        } else if (spawn.what == SPAWN_TYPE_SEEKER) {
            this.factory.createSeeker(this.scene, spawn.where);
        }
    }
}