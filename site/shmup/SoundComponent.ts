import { GenericComponent } from "../../ts/components/GenericComponent";
import { MSG_PLAYER_BULLET_SHOT, MSG_GAME_OVER, MSG_ENEMY_HIT, MSG_PLAYER_HIT, SOUND_FIRE, SOUND_KILL, SOUND_LIFE_LOST, SOUND_GAMEOVER, SOUND_STAGE_START, MSG_STAGE_START } from "./Constants";

/**
 * A component to play sounds based on various messages.
 */
export class SoundComponent extends GenericComponent {

    constructor(){
        super(SoundComponent.name);

        // the generic component provides a more concise interface for this usage
        this.doOnMessage(MSG_PLAYER_BULLET_SHOT, (cmp, msg) => (<any>PIXI.loader.resources[SOUND_FIRE]).sound.play());
        this.doOnMessage(MSG_GAME_OVER, (cmp, msg) => (<any>PIXI.loader.resources[SOUND_GAMEOVER]).sound.play());
        this.doOnMessage(MSG_ENEMY_HIT, (cmp, msg) => (<any>PIXI.loader.resources[SOUND_KILL]).sound.play());
        this.doOnMessage(MSG_PLAYER_HIT, (cmp, msg) => (<any>PIXI.loader.resources[SOUND_LIFE_LOST]).sound.play());
        this.doOnMessage(MSG_STAGE_START, (cmp, msg) => (<any>PIXI.loader.resources[SOUND_STAGE_START]).sound.play());
    }
}