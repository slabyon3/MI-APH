import Component from "../../ts/engine/Component";
import { PIXICmp } from "../../ts/engine/PIXIObject";
import { Sprite } from "pixi.js";
import { checkTime } from "./Utils";
import { updateJsxFragment } from "typescript";
import { ANIMATION_EXPLOSION_FRAME_SIZE } from "./Constants";

/**
 * Does horizontally prepared animation.
 * Expects the frame to already be aligned to the first animation frame.
 */
export class AnimationComponent extends Component {
    frames: number;
    currentFrame: number = 0;
    animationSpeed: number;
    animationTime: number = 0;
    finishOnAnimDone: boolean;

    texture: PIXI.Texture;

    constructor(
        frames: number,
        animationSpeed: number,
        finishOnAnimDone: boolean = false
    ) {
        super();
        this.frames = frames;
        this.animationSpeed = animationSpeed;
        this.finishOnAnimDone = finishOnAnimDone;
    }

    onInit() {
        super.onInit();

        // cache our current texture
        var sprite = <PIXICmp.Sprite>this.owner;
        this.texture = sprite.texture;
    }

    onUpdate(delta: number, absolute: number) {
        // animate at a speed according to the animation speed setting
        if (checkTime(this.animationTime, absolute, this.animationSpeed)) {
            this.animationTime = absolute;

            // frames loop
            this.currentFrame = (this.currentFrame + 1) % this.frames;
            if (this.currentFrame == 0 && this.finishOnAnimDone) {
                this.finish(); // if we should just be a one-time animation, finish when done animating
                return;
            }
            this.updateTextureFrame();
        }
    }

    updateTextureFrame() {
        // compute the new sprite frame and apply it to our texture
        var frame = new PIXI.Rectangle(this.currentFrame * ANIMATION_EXPLOSION_FRAME_SIZE, 0, ANIMATION_EXPLOSION_FRAME_SIZE, ANIMATION_EXPLOSION_FRAME_SIZE);
        this.texture.frame = frame;
    }
}