import Vec2 from "../../ts/utils/Vec2";


export const SPAWN_TYPE_MISSILE = 'missile';
export const SPAWN_TYPE_SEEKER = 'seeker';
export const SPAWN_TYPE_FISH = 'fish';
export const SPAWN_TYPE_ASTEROID = 'asteroid';

/**
 * A class to represent information for the spawner
 * on when, what and where to spawn.
 */
export class SpawnInfo {
    when: number;
    what: string;
    where: Vec2;
}